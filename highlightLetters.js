// не рекомендуется использовать события клавиатуры для input'а потому что мы не сможем контролировать всё то что 
// пользователь введёт в input без событий клавиатуры (вставка готового текста)
const button = document.querySelectorAll(".btn") //добавил .onkeydown для shift
document.onkeydown = function(event) {
    button.forEach(element => {
        element.dataset.buttonKey === event.key ? element.classList.add("active") : element.classList.remove("active")
    })
}


// цвет пропадает после отпуксания
// const button = document.querySelectorAll(".btn")
// document.onkeydown = function(event) {
//     button.forEach(element => {
//         element.dataset.buttonKey === event.key ? element.classList.add("active") : false
//     })
// }
// document.onkeyup = function() {
//     button.forEach(element => {
//         element.classList.remove("active")
//     })
// }